package com.example.lenovo.blinckingactitity;

import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.view)
    View view;
    final int[] colors = {0xffff0000, 0xff00ff00, 0xff0000ff, 0xffff4488};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        new Thread()
        {
            int currentColor = 0;
            int step = 1;
            public void run() {
                while (true){
                    view.setBackgroundColor(colors[currentColor]);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    currentColor += step;
                    if (currentColor + step >= colors.length || currentColor + step < 0) {
                        step = -step;
                    }
                    if (colors.length == 1) {
                        step = 0;
                    }
                }
            }
        }.start();
    }

}
